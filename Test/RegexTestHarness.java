import java.io.Console;
import java.util.*;
import java.io.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class RegexTestHarness {

    public static void main(String[] args) throws FileNotFoundException{
        Console console = System.console();
        Scanner input = new Scanner(new File(args[0]));
        if (console == null || args.length != 1 ) {
            System.err.println("No console.");
            System.exit(1);
        }
        String file = "";
        while(input.hasNext()){
            file += input.nextLine();
        }
        Pattern pattern = 
        Pattern.compile(console.readLine("%nEnter your regex: "));
        
        Matcher matcher = 
        pattern.matcher(file);

        boolean found = false;
        while (matcher.find()) {
            console.format("I found the text" +
                " \"%s\" starting at " +
                "index %d and ending at index %d.%n",
                matcher.group(),
                matcher.start(),
                matcher.end());
            found = true;
        }
        if(!found){
            console.format("No match found.%n");
        }
    }
}